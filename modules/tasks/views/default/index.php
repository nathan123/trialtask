<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 4:15 PM
 */
use yii\grid\GridView;

$this->title = 'DATA LIST';

echo '<h1>Category List</h1>';
echo GridView::widget([
    'dataProvider' => $category,
]);

echo '<h1>Retailers List</h1>';
echo GridView::widget([
    'dataProvider' => $retailers,
]);

echo '<h1>Child Category List</h1>';
echo GridView::widget([
    'dataProvider' => $categorySelect,
]);

echo '<h1>Product List</h1>';
echo GridView::widget([
    'dataProvider' => $productCategory,
]);

echo '<h1>Specific Product List</h1>';
echo GridView::widget([
    'dataProvider' => $specificProductcategory,
]);
?>
