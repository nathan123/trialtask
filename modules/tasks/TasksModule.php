<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 4:13 PM
 */
namespace app\modules\tasks;

class TasksModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\tasks\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}