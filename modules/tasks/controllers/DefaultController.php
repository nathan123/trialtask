<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 4:25 PM
 */

namespace app\modules\tasks\controllers;

use yii\base\Controller;
use yii\data\ActiveDataProvider;
use app\models\Category;
use app\models\Product;
use app\models\Retailers;

class DefaultController extends Controller
{
    /**
     * @return index view
     */
    public function actionIndex()
    {   
        // $prod = new Product;
        // echo '<br/>';
        // print_r($prod->getProductImages()->all());
        /**
         * @return ActiveDataProvider Category List
         */
        $category = new ActiveDataProvider([
            'query' => Category::find(),
            'pagination' => [
                'pageSize' => 50,
                'defaultPageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        /**
         * @return ActiveDataProvider Category List With Retailers
         */    
        $retailer = new ActiveDataProvider([
            'query' => Retailers::getRetailers(),
            'pagination' => [
                'pageSize' => 50,
                'defaultPageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        /**
         * @return ActiveDataProvider getFashionCategory
         */

        $categorySelect = new ActiveDataProvider([
            'query' => Category::getFashionCategory(),
            'pagination' => [
                'pageSize' => 50,
                'defaultPageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);


        /**
         * @return ActiveDataProvider getFashionCategory
         */

        $productCategory = new ActiveDataProvider([
            'query' => Product::getProductCategory(),
            'pagination' => [
                'pageSize' => 50,
                'defaultPageSize' => 50,
            ]
        ]);

        /**
         * @return ActiveDateProvider getSpecificProductCategory
         */
        $specificProductcategory = new ActiveDataProvider([
            'query' => Product::getSpecificProductCategory(),
            'pagination' => [
                'pageSize' => 50,
                'defaultPageSize' => 50,
            ]
        ]);
        
        return $this->render('index',[
            'category' => $category,
            'categorySelect' => $categorySelect,
            'retailers' => $retailer,
            'productCategory' => $productCategory,
            'specificProductcategory' => $specificProductcategory,
        ]);
    }
}