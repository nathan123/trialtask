<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 1:59 PM
 */

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class Category extends ActiveRecord
{
    const PROGRAM_ID = '9';
    const CATEGORY_FASHION = '3';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function getCategory()
    {
        return self::find();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id', 'name'], 'required'],
            [['program_id', 'parent_id'], 'integer'],
            [['name'], 'string'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Category Name',
        ];
    }

    /**
     * @return Object
     */
    public static function getFashionCategory($category = null)
    {
        $query = self::find()
                ->distinct(true)
                ->leftJoin('retailer_category_association', 'retailer_category_association.category_id = category.id')
                ->leftJoin('retailers', 'retailer_category_association.retailer_id = retailers.id');

        if ($category)
        {
            $query->where(['parent_id' => $category]);
        }
        else
        {
            $query->where(['parent_id' => self::CATEGORY_FASHION]);
        }

        return $query;
    }
}