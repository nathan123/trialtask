<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 2:01 PM
 */

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Category;

class Retailers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'retailers';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name','rname'], 'string'],
            [['name','rname'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Retailers Name',
            'rname' => 'Retailers Name',
        ];
    }

    /**
     * @return Object
     */
    public static function getRetailers($program_id=null)
    {
        $query = self::find()
                ->distinct(true)
                ->leftJoin('retailer_category_association', 'retailer_category_association.retailer_id = retailers.id')
                ->leftJoin('retailer_program_association', 'retailer_program_association.retailer_id = retailers.id')
                ->leftJoin('category', 'category.id = retailer_category_association.category_id');

        if ($program_id)
        {
            $query->where(['category.program_id' => $program_id]);
        }
        else
        {
            $query->where(['category.program_id' => Category::PROGRAM_ID]);
        }

        return $query;
    }
}