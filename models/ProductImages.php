<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 2:01 PM
 */
namespace app\models;

use yii\db\ActiveRecord;
use app\models\Product;

class ProductImages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_images';
    }

    public function rules()
    {
        return [
            [['product_id', 'retailer_id'], 'required'],
            [['image_url'], 'string'],
            [['image_url'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'image_url' => Yii::t('Image URL'),
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(),['id' => 'product_id']);
    }
}