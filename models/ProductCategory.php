<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 2:01 PM
 */

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Product;

class ProductCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    public function rules()
    {
        return [
           [['product_category_name'], 'required'],
           [['product_category_name'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'product_category_name' => Yii::t('Product Category Name'),
        ];
    }

    public function getProduct()
    {
        return $this->hasMany(Product::className(),['id' => 'product_category_id']);
    }
}