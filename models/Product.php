<?php
/**
 * Created by PhpStorm.
 * User: Cloud301
 * Date: 8/17/2016
 * Time: 2:00 PM
 */

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Category;
use app\models\ProductCategory;
use YIi;

class Product extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }
    
    public function rules()
    {
        return [
            [
                [
                    'product_name',
                    'price',
                    'product_category_id',
                    'retail_id',
                    'stock',
                ],
                'required'
            ],
            [['product_name', 'currency', 'stock'], 'string'],
            [['product_name','currency','stock'], 'safe'],
            [['product_category_id', 'retail_id'], 'integer'],
            [['price', 'retail_price'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'product_name' => 'Product Name',
            'price' => 'Price',
            'retail_price' => 'Retail Price',
            'currency' => 'Currency',
            'stock' => 'Stock',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['product_category_id' => 'id']);
    }

    public function getProductImages()
    {
        return $this->hasOne(ProductImages::className(), ['product_id' => 'id']);
    }

    public static function getProductCategory($program_id = null)
    {   
        $query = self::find()
                    ->select([
                            'product.*',
                            'product_images.image_url'
                        ])
                    ->distinct(true)
                    // ->joinWith(['product_category', 'product_category.id = product.product_category_id'], false, 'LEFT JOIN')
                    // ->joinWith(['product_images', 'product_images.product_id = product.id'], false, 'LEFT JOIN')
                    // ->joinWith(['category_to_product_category_association','category_to_product_category_association.product_category_id = product_category.id'], false, 'LEFT JOIN')
                    ->leftJoin('product_category', 'product_category.id = product.product_category_id')
                    ->leftJoin('product_images', 'product_images.product_id = product.id')
                    ->leftJoin('category_to_product_category_association','category_to_product_category_association.product_category_id = product_category.id')
                    ->leftJoin('category', 'category.id = category_to_product_category_association.category_id')
                    ->orderBy(['product.id' => SORT_ASC]);

        if ($program_id)
        {
            $query->where(['category.program_id' => $program_id]);
        }
        else
        {
            $query->where(['category.program_id' => Category::PROGRAM_ID]);
        }

        return $query;
    }

    public static function getSpecificProductCategory($program_id = null, $parent_id = null)
    {
        $query = self::find()
            ->distinct(true)
            ->leftJoin('product_category', 'product_category.id = product.product_category_id')            
            ->leftJoin('category_to_product_category_association','category_to_product_category_association.product_category_id = product_category.id')
            ->leftJoin('category', 'category.id = category_to_product_category_association.category_id')
            ->where(['category.parent_id' =>  Category::CATEGORY_FASHION]);

        if ($program_id)
        {
            $query->where(['category.program_id' => $program_id]);
        }
        else
        {
            $query->where(['category.program_id' => Category::PROGRAM_ID]);
        }

        if ($parent_id)
        {
            $query->andWhere(['category_to_product_category_association.category_id' => $parent_id]);
        }
        else
        {
            $query->andWhere(['category_to_product_category_association.category_id' => Category::CATEGORY_FASHION]);
        }

        return $query;
    }

}