<?php

use yii\db\Migration;

class m160817_051028_user extends Migration
{
    public function up()
    {
        $this->createTable('users', array(
            'id' => 'pk',
            'email' => 'string NOT NULL',
            'username' => 'string NOT NULL',
            'password' => 'string NOT NULL',            
            'created_at' => 'DATETIME',            
            'updated_at' => 'DATETIME',            
        ), 'ENGINE=InnoDB CHARSET=utf8');
    }

    public function down()
    {
        echo "m160817_051028_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
